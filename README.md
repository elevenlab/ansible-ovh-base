Ansible Role: Ovh-base
===================
Configure Network enviroment in the [OVH Public Cloud](https://www.ovh.com/cloud/). **Works only for Debian 8 provided machine**

It will disable the ovh automatic managment of `/etc/hosts` and `hostname`* on the machines. It also configure and bring up additional netwrok interfaces, both public (`ip failover`) and private (`vLan`)

Both `ip failover` and `vLan` must be enabled and attached to the machine either via the ovh control panel or ovh-API. This role only enable these interfaces on the machines

----------
\* **Please note** that *reinstalling* the machine the automatic managment of *hosts* and *hostname* will be re-enabled by ovh. Thus take particularly care changing the `hostname` (see the TODOs: at the moment you can't actually change the `hostname` with this role, but you can set an `hostname` for an IP/interface)

Requirements
-----------------
Ovh machine
Server must be **Debian 8** provided image by ovh inside the **Public Cloud** Infrastructure

Role variables
-------------

Available variables are listed below, along with default values (see `defaults/main.yml`):

	ovh_private_interface:
	    interface: eth1
	    ip: '0.0.0.0'
	    mask: 255.255.255.0
	    hostname: 'ovh-server'

Defining a private interface is mandatory (see TODOs) it will create an interface directive inside `/etc/network/interfaces.d` called `{{ovh_private_interface.interface}}.cfg`, setting static ip and submask. Furthermore it will add in `/etc/hosts` an host `{{ovh_private_interface.ip}} {{ovh_private_interface.hostname}}.localdomain {{ovh_private_interface.hostname}}`.

**Please note:** if this role is used different time with different private_interface. Be sure that hostname are different (otherwise need to change this role) 

	ovh_public_interfaces: []
A list of all additional public interfaces that have to be configured and brought up, subvar are:

	- interface: eth0:0
      ip: 'xxx.xxx.xxx.xxx' 
      mask: 255.255.255.255
If the element exists, all the fields are mandatory.

	ovh_hosts: []
A list of external host that will added to `/etc/hosts`, designed to easly resolv host inside the private network. For each host variable are:

	- hostname: 'hostname'
      ip: '10.0.0.11'
If the element exists, both the fields are mandatory.
   
Dependencies
-------------------
None.

Example Playbook
--------------------------
    - name: config ovh network 
      hosts: all
      vars:
        ovh_private_interface:
            interface: eth1
            ip: '{{ private_ip }}'
            hostname: '{{ ansible_hostname }}'
            mask: '255.255.255.0'
        
        ovh_public_interfaces:
            - interface: eth0:0
              ip: '{{ip_failover}}' 
              mask: 255.255.255.255

        ovh_hosts: []
      pre_tasks:
        - set_fact:
             ovh_hosts: "{{ ovh_hosts }} + [ { 'hostname': '{{ item }}', 'ip': '{{ hostvars[item].private_ip }}' } ]"   
          with_items: '{{ groups["all"] | difference([ansible_hostname]) }}'
      roles:
        - ovh-base
where `private_ip` and `ip_failover` are defined for each host

TODO
-------------
* Allow to change `hostname` of the machine
* The role should also works if not `private_interface` is provided (e.g. no private interface needed on the machine)